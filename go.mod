module (
 github.com/gabriellmandelli/seed-go-service
)

go 1.13

require (
	github.com/go-delve/delve v1.5.1 // indirect
	github.com/google/uuid v1.1.2
	github.com/labstack/echo/v4 v4.1.17
	github.com/stretchr/testify v1.4.0
)
