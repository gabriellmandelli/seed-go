run: ## run the API server
	go run cmd/server/main.go

build: ## run the API server
	go build cmd/server/main.go
