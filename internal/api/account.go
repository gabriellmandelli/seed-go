package api

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

type Account struct {
}

func NewAccount() *Account {
	return &Account{}
}

const (
	accountUrl = "/account"
)

func (controller *Account) Register(server *echo.Echo) {
	v1 := server.Group("v1")
	v1.GET(accountUrl, controller.findAccountById)
	v1.POST(accountUrl, controller.createAccount)
	v1.POST(accountUrl+"/:accountId", controller.updateAccount)
	v1.DELETE(accountUrl, controller.deleteAccountById)
}

func (controller *Account) findAccountById(context echo.Context) error {
	return context.JSON(http.StatusOK, "")
}

func (controller *Account) createAccount(context echo.Context) error {
	return context.JSON(http.StatusCreated, "")
}

func (controller *Account) updateAccount(context echo.Context) error {
	return context.JSON(http.StatusOK, "")
}

func (controller *Account) deleteAccountById(context echo.Context) error {
	return context.JSON(http.StatusNoContent, "")
}
