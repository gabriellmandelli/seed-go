package model

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

const modelAccountExample = `{
	"account_id":"a100bef6-477e-11eb-b378-0242ac130002",
	"name":"Gabriel",
	"email":"gabriel-mandelli@hotmail.com",
	"phone":"48999999999"
}
`

func TestConvertBodyToModel(t *testing.T) {

	accountModel := Account{}

	err := json.Unmarshal([]byte(modelAccountExample), &accountModel)

	assert.Nil(t, err)
	assert.Equal(t, accountModel.AccountId, "a100bef6-477e-11eb-b378-0242ac130002")
	assert.Equal(t, accountModel.Email, "gabriel-mandelli@hotmail.com")
	assert.Equal(t, accountModel.Name, "Gabriel")
	assert.Equal(t, accountModel.Phone, "48999999999")
}
