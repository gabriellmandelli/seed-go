package model

type Account struct {
	AccountId string `json:"account_id"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
}
